# -*- cmake -*-
cmake_minimum_required(VERSION 2.8)

project(google_breakpad)

##message(STATUS "CMAKE_CXX_FLAGS = '${CMAKE_CXX_FLAGS}'")
##string(REGEX MATCHALL "[^ ]+" FLAGSLIST "${CMAKE_CXX_FLAGS}")
##message(STATUS "FLAGSLIST = '${FLAGSLIST}'")

IF(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    string(REGEX MATCH "-mmacosx-version-min=([^ ]+)" scratch "${CMAKE_CXX_FLAGS}")
    set(CMAKE_OSX_DEPLOYMENT_TARGET "${CMAKE_MATCH_1}")
    set(CMAKE_XCODE_ATTRIBUTE_GCC_VERSION "com.apple.compilers.llvm.clang.1_0")
    set(CMAKE_OSX_ARCHITECTURES "$ENV{AUTOBUILD_CONFIGURE_ARCH}")
    set(CMAKE_CXX_COMPILER "clang++")
    set(CMAKE_C_COMPILER "clang++")
    add_subdirectory("client/mac/handler")

    message(STATUS "CMAKE_OSX_DEPLOYMENT_TARGET = '${CMAKE_OSX_DEPLOYMENT_TARGET}'")
    message(STATUS "CMAKE_OSX_ARCHITECTURES = '${CMAKE_OSX_ARCHITECTURES}'")
    message(STATUS "CMAKE_CXX_FLAGS = '${CMAKE_CXX_FLAGS}'")
    message(STATUS "CMAKE_C_FLAGS   = '${CMAKE_C_FLAGS}'")

ENDIF(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
