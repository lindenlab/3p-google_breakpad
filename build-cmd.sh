#!/usr/bin/env bash

#As of this writing, google breakpad does not have a version number.
#We therefor will use the SVN checkout revision of the latest checkout from google:
#       svn info | grep "Revision" | awk '{print $2}'
#The following VERSION string will need to be updated manually.
version=1413		#svn revision as of 2014-10-29

cd "$(dirname "$0")"

# turn on verbose debugging output for parabuild logs.
exec 4>&1; export BASH_XTRACEFD=4; set -x
# make errors fatal
set -e
# complain about unset env variables
set -u

if [ -z "$AUTOBUILD" ] ; then
    exit 1
fi

# load autobuild provided shell functions and variables
# first remap the autobuild env to fix the path for sickwin
if [ "$OSTYPE" = "cygwin" ] ; then
    autobuild="$(cygpath -u $AUTOBUILD)"
else
    autobuild="$AUTOBUILD"
fi

stage="$(pwd)/stage"

source_environment_tempfile="$stage/source_environment.sh"
"$autobuild" source_environment > "$source_environment_tempfile"
. "$source_environment_tempfile"

LIBRARY_DIRECTORY_RELEASE="$stage/lib/release"
BINARY_DIRECTORY="$stage/bin"
INCLUDE_DIRECTORY="$stage/include/google_breakpad"
mkdir -p "$LIBRARY_DIRECTORY_RELEASE"
mkdir -p "$BINARY_DIRECTORY"

# We find ourselves reproducing portions of the include tree into
# $INCLUDE_DIRECTORY. This shell function takes as its first argument the
# partial directory path, and as its second the file(s) to copy, which may
# include wildcards. It first ensures the existence of the partial directory
# path under $INCLUDE_DIRECTORY, then copies the file(s) indicated.
function copyh {
    # copyh partial/path wildcard.h
    # from src/partial/path/wildcard.h to $INCLUDE_DIRECTORY/partial/path/
    # ensure destination directory exists
    mkdir -p "$INCLUDE_DIRECTORY/$1"
    # copy from src/partial/path to $INCLUDE_DIRECTORY/partial/path
    # don't quote $2 so shell can expand wildcards, if any
    cp src/"$1"/$2 "$INCLUDE_DIRECTORY/$1/"
}

case "$AUTOBUILD_PLATFORM" in
    # ------------------------------- windows --------------------------------
    windows*)
        case "$AUTOBUILD_VSVER" in
            "120")
                msvs_version="2013"
                sdk_version=""
                ;;
            "150")
                msvs_version="2017"
                sdk_version="10.0.17134.0"
                ;;
            *)
                echo "Unknown AUTOBUILD_VSVER='$AUTOBUILD_VSVER'" 1>&2 ; exit 1
                ;;
        esac

        # TODO: never mind patching, use
        # https://wiki.lindenlab.com/wiki/Mercurial_Vendor_Branches !

        # patch vcproj generator to use Multi-Threaded DLL for +3 link karma
        #
        # modified gyp is checked in now...patch is in repo for reference
        #
        # patch -p 1 < gyp.patch
        (
            cd src/client/windows
            # LL_BUILD_RELEASE_MACROS contains (e.g.) /DWIN32 /DLL_WINDOWS=1
            # which is almost the form in which the gyp script can accept
            # them, replacing /D with -D. So use bash substitution syntax to
            # replace every /D with -D. Don't quote the result because we want
            # gyp to see them as separate arguments.
            # On the other hand, try passing all of LL_BUILD_RELEASE_SWITCHES
            # as the value of gyp's 'cflags' settings variable.
            ../../tools/gyp/gyp --no-circular-check -f msvs -G msvs-version="$msvs_version" \
                ${LL_BUILD_RELEASE_MACROS///D/-D} \
                -Dcflags="$LL_BUILD_RELEASE_SWITCHES"
        )

        load_vsvars

        # We don't use build_sln here because we need /Upgrade rather than
        # /build, which is the only action supported in build_sln.
        devenv.com src/client/windows/breakpad_client.sln /Upgrade
        devenv.com src/tools/windows/dump_syms/dump_syms.vcxproj /Upgrade

        # For some reason gyp is generating breakpad_client.sln's .vcxproj
        # files without
        # <WindowsTargetPlatformVersion>$sdk_version</WindowsTargetPlatformVersion>
        # which they should have with VS 2017. Fix that by adding the relevant
        # property.
        sedscript="$stage/fixversion.sed"
        echo > "$sedscript" "/<PropertyGroup Label=\"Globals\">/a\
    <WindowsTargetPlatformVersion>$sdk_version</WindowsTargetPlatformVersion>
"

        for vcxproj in \
            handler/exception_handler \
            crash_generation/crash_generation_{client,server} \
            common
        do
            if [ -n "$sdk_version" ]
            then sed -E -i.bak -f "$sedscript" "src/client/windows/$vcxproj.vcxproj"
            fi
            proj="$(basename "$vcxproj")"
            build_sln src/client/windows/breakpad_client.sln "Release|$AUTOBUILD_WIN_VSPLATFORM" "$proj"
        done

        # avoid USE_INCREDIBUILD - buildconsole doesn't support building vcxproj files directly, yet
        vcxproj="src/tools/windows/dump_syms/dump_syms.vcxproj"
        if [ -n "$sdk_version" ]
        then sed -E -i.bak -f "$sedscript" "$vcxproj"
        fi
        # For reasons that baffle and dismay me, "Release|Win32" does NOT seem
        # to work with dump_syms.vcxproj, but plain "Release" gets Win32 by
        # default.
        if [ "$AUTOBUILD_WIN_VSPLATFORM" == "Win32" ]
        then config="Release"
        else config="Release|$AUTOBUILD_WIN_VSPLATFORM"
        fi
        build_sln "$vcxproj" "$config"

        mkdir -p "$INCLUDE_DIRECTORY"
        cp ./src/client/windows/handler/exception_handler.h "$INCLUDE_DIRECTORY"
        copyh client/windows/common "*.h"
        copyh client/windows/crash_generation "*.h"
        copyh common/windows "*.h"
        copyh google_breakpad/common "*.h"
        copyh processor "scoped_ptr.h"
        copyh common "scoped_ptr.h"

        if [ "$AUTOBUILD_ADDRSIZE" = 32 ]
        then bindir=Release
        else bindir=x64/Release
        fi
        cp ./src/client/windows/Release/lib/*.lib "$LIBRARY_DIRECTORY_RELEASE"
        cp ./src/tools/windows/dump_syms/$bindir/dump_syms.exe "$BINARY_DIRECTORY"
    ;;

    # -------------------------------- darwin --------------------------------
    darwin*)
        (
            cd src/
            rm CMakeCache.txt
            FLAGS="-m$AUTOBUILD_ADDRSIZE $LL_BUILD_RELEASE"
            cmake -G Xcode CMakeLists.txt \
                "-DCMAKE_CXX_FLAGS=$FLAGS" "-DCMAKE_C_FLAGS=$FLAGS"
            # On Mac, this build requires -lcrypto. Apparently we used to find
            # that on the build system... We've introduced an explicit
            # dependency on our openssl autobuild package, which has been
            # installed into $stage/packages/lib/release/libcrypto.a.
            # Note: '${inherited}' must pass through to be expanded by
            # xcodebuild itself -- don't let bash try to expand it!
            xcodebuild -arch "$AUTOBUILD_CONFIGURE_ARCH" -project google_breakpad.xcodeproj \
                       -configuration Release \
                       LIBRARY_SEARCH_PATHS="\${inherited} $stage/packages/lib/release"
        )

        (
            # dump_syms.xcodeproj contains lines of the form:
            # SDKROOT = macosx10.9;
            # producing errors like this:
            # error: There is no SDK with the name or path 'src/tools/mac/dump_syms/macosx10.9'

            # First, find what our -iwithsysroot SHOULD be.
            # Set this subshell's positional parameters from $LL_BUILD_RELEASE, using
            # -- because the first switch in $LL_BUILD_RELEASE probably starts with
            # '-', which confuses 'set'.
            set -- $LL_BUILD_RELEASE
            sdk=""
            while [ "${1:-}" ]
            do if [ "$1" == "-iwithsysroot" ]
               then sdk="$2"
                    break
               fi
               # the point of using positional parameters is using shift to
               # iterate through them
               shift
            done

            cd src/tools/mac/dump_syms

            xcodebuild -arch "$AUTOBUILD_CONFIGURE_ARCH" -project dump_syms.xcodeproj \
                -configuration Release \
                SDKROOT="$sdk"
        )


##      mkdir -p "$INCLUDE_DIRECTORY/google_breakpad/common"
        mkdir -p "$INCLUDE_DIRECTORY/client/mac/crash_generation/common/mac"

        cp ./src/common/mac/MachIPC.h "$INCLUDE_DIRECTORY/client/mac/crash_generation/common/mac"
        cp ./src/client/mac/handler/exception_handler.h "$INCLUDE_DIRECTORY"
        copyh client/mac/handler "ucontext_compat.h"
        copyh client/mac/crash_generation "crash_generation_client.h"
        copyh processor "scoped_ptr.h"
        copyh common "scoped_ptr.h"

        cp ./src/client/mac/handler/Release/libexception_handler.dylib "$LIBRARY_DIRECTORY_RELEASE"
        cp ./src/tools/mac/dump_syms/build/Release/dump_syms "$BINARY_DIRECTORY"
    ;;

    # -------------------------------- linux ---------------------------------
    linux*)
        VIEWER_FLAGS="-m$AUTOBUILD_ADDRSIZE -fno-stack-protector $LL_BUILD_RELEASE"

        # It's bad to freeze a particular gcc version into this build-cmd.sh
        # file. We've encountered several such version preferences in other
        # repositories, some of which were obsolete and wrong. On the other
        # hand, as of 2016-03-23, the Linux build host we're using somehow
        # seizes on /usr/bin/gcc-4.1 -- despite the fact that plain 'gcc' or
        # 'g++' is version 4.6.3! Try overriding whatever mischief is in the
        # environment.
        ./configure --prefix="$stage" \
            CC="gcc" CXX="g++" \
            CFLAGS="$VIEWER_FLAGS" CXXFLAGS="$VIEWER_FLAGS" LDFLAGS="$VIEWER_FLAGS"
        make
        make -C src/tools/linux/dump_syms/ dump_syms
        make install

        # replicate breakpad headers
        copyh common "*.h"
        copyh google_breakpad/common "*.h"

        # no really all of them
        copyh client/linux/crash_generation "*.h"
        copyh client/linux/dump_writer_common "*.h"
        copyh client/linux/handler "*.h"
        copyh client/linux/minidump_writer "*.h"
        copyh client/linux/log "*.h"
        copyh third_party/lss "*"
        copyh processor "scoped_ptr.h"
        copyh common "scoped_ptr.h"

        # and then cherry-pick some so they are found as used by linden
        cp src/client/linux/handler/*.h "$INCLUDE_DIRECTORY"
        cp src/common/using_std_string.h "$INCLUDE_DIRECTORY"
        cp src/client/linux/handler/exception_handler.h "$INCLUDE_DIRECTORY"
        cp src/client/linux/handler/exception_handler.h "$INCLUDE_DIRECTORY/google_breakpad/"
        cp src/client/linux/handler/minidump_descriptor.h "$INCLUDE_DIRECTORY"
        cp src/client/linux/handler/minidump_descriptor.h "$INCLUDE_DIRECTORY/google_breakpad/"

        # libs and binaries
        cp -P "$stage/lib"/libbreakpad*.a* "$LIBRARY_DIRECTORY_RELEASE"
        cp src/tools/linux/dump_syms/dump_syms "$BINARY_DIRECTORY"
    ;;
esac

#Write phony version number into version file.
build=${AUTOBUILD_BUILD_ID:=0}
echo "${version}.${build}" > "${stage}/version.txt"

# yes, this looks dumb, no, it's not incorrect
mkdir -p "$stage/LICENSES"
cp COPYING "$stage/LICENSES/google_breakpad.txt"
